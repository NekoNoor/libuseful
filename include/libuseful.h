/* ************************************************************************** */
/*                                                                            */
/*                                                             ::::::::       */
/*   libuseful.h                                             :+:    :+:       */
/*                                                          +:+               */
/*   By: nekonoor <nekonoor@protonmail.com>                +#+                */
/*                                                        +#+                 */
/*   Created: 2019/09/04 17:31:28 by nekonoor            #+#    #+#           */
/*   Updated: 2019/09/04 17:37:47 by nekonoor            ########   odam.nl   */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBUSEFUL_H
# define LIBUSEFUL_H

void	uf_bzero(void *ptr, int len);
void	*uf_memset(void *ptr, char ch, int len);

void	uf_putchar(char ch);
void	uf_putstr(char *str);
void	uf_putnbr(int nbr);
void	uf_puthex(unsigned char ch);

int		uf_strlen(char *str);
char	*uf_strcpy(char *dest, char *src);
char    *uf_strdup(char *src);
char	*uf_strrev(char *str);
char	*uf_strcat(char *dest, char *src);

int		uf_numlen_base(int nbr, int base);
char	*uf_ctoa(unsigned char nbr);
char	uf_atoc(char *str);
int		uf_atoi_base(char *str, int base);
char	*uf_itoa_base(int nbr, int base);

#endif
