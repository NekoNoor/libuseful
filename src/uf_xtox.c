/* ************************************************************************** */
/*                                                                            */
/*                                                             ::::::::       */
/*   uf_xtox.c                                               :+:    :+:       */
/*                                                          +:+               */
/*   By: nekonoor <nekonoor@protonmail.com>                +#+                */
/*                                                        +#+                 */
/*   Created: 2019/09/04 17:30:48 by nekonoor            #+#    #+#           */
/*   Updated: 2019/09/04 17:42:18 by nekonoor            ########   odam.nl   */
/*                                                                            */
/* ************************************************************************** */

#include "libuseful.h"
#include <stdlib.h>

int		uf_numlen_base(int nbr, int base)
{
	int	len;

	len = 0;
	if (nbr == 0)
		len++;
	while (nbr)
	{
		nbr /= base;
		len++;
	}
	return (len);
}

char	*uf_ctoa(unsigned char nbr)
{
	char	*str;
	int		i;

	str = (char*)malloc(sizeof(char) * (uf_numlen_base(nbr, 10) + 1));
	if (str == NULL)
		return (NULL);
	i = 0;
	if (nbr == 0)
		str[i] = nbr % 10 + '0';
	while (nbr)
	{
		str[i] = nbr % 10 + '0';
		nbr /= 10;
		i++;
	}
	str[i] = '\0';
	return(uf_strrev(str));
}

char	uf_atoc(char *str)
{
	char	nbr;
	int		i;

	nbr = 0;
	i = 0;
	while (*str >= '0' && *str <= '9' && i < 3)
	{
		nbr = nbr * 10 + *str - '0';
		str++;
		i++;
	}
	return (nbr);
}

int		uf_atoi_base(char *str, int base)
{
	(void)str;
	(void)base;
	return (0);
}

char	*uf_itoa_base(int nbr, int base)
{
	(void)nbr;
	(void)base;
	return (NULL);
}
