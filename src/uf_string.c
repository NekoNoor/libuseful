/* ************************************************************************** */
/*                                                                            */
/*                                                             ::::::::       */
/*   uf_string.c                                             :+:    :+:       */
/*                                                          +:+               */
/*   By: nekonoor <nekonoor@protonmail.com>                +#+                */
/*                                                        +#+                 */
/*   Created: 2019/09/04 17:28:05 by nekonoor            #+#    #+#           */
/*   Updated: 2019/09/04 17:33:45 by nekonoor            ########   odam.nl   */
/*                                                                            */
/* ************************************************************************** */

#include "libuseful.h"
#include <stdlib.h>

int		uf_strlen(char *str)
{
	int	len;

	len = 0;
	while (str[len])
		len++;
	return (len);
}

char	*uf_strcpy(char *dest, char *src)
{
	int i;

	i = 0;
	while (src[i])
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

char    *uf_strdup(char *src)
{
	char    *dup;

	dup = (char*)malloc(sizeof(char) * (uf_strlen(src) + 1));
	dup = uf_strcpy(dup, src);
	return (dup);
}

char	*uf_strrev(char *str)
{
	char	swap;
	int		len;
	int		i;

	len = uf_strlen(str);
	i = 0;
	while (i < len / 2)
	{
		swap = str[i];
		str[i] = str[len - i - 1];
		str[len - i - 1] = swap;
		i++;
	}
	return (str);
}

char	*uf_strcat(char *dest, char *src)
{
	uf_strcpy(dest + uf_strlen(dest), src);
	return (dest);
}
