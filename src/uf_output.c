/* ************************************************************************** */
/*                                                                            */
/*                                                             ::::::::       */
/*   uf_output.c                                             :+:    :+:       */
/*                                                          +:+               */
/*   By: nekonoor <nekonoor@protonmail.com>                +#+                */
/*                                                        +#+                 */
/*   Created: 2019/09/04 17:29:00 by nekonoor            #+#    #+#           */
/*   Updated: 2019/09/04 17:41:17 by nekonoor            ########   odam.nl   */
/*                                                                            */
/* ************************************************************************** */

#include "libuseful.h"
#include <unistd.h>

void	uf_putchar(char ch)
{
	write(1, &ch, 1);
}

void	uf_putstr(char *str)
{
	write(1, str, uf_strlen(str));
}

void	uf_putnbr(int nbr)
{
	if (nbr == -2147483648)
	{
		uf_putnbr(-214748364);
		uf_putchar('8');
		return ;
	}
	if (nbr < 0)
	{
		uf_putchar('-');
		nbr *= -1;
	}
	if (nbr < 10)
		uf_putchar(nbr + '0');
	else
	{
		uf_putnbr(nbr / 10);
		uf_putnbr(nbr % 10);
	}
}

void	uf_puthex(unsigned char ch)
{
	(void)ch;
}
