/* ************************************************************************** */
/*                                                                            */
/*                                                             ::::::::       */
/*   uf_memory.c                                             :+:    :+:       */
/*                                                          +:+               */
/*   By: nekonoor <nekonoor@protonmail.com>                +#+                */
/*                                                        +#+                 */
/*   Created: 2019/09/04 17:30:12 by nekonoor            #+#    #+#           */
/*   Updated: 2019/09/04 17:30:28 by nekonoor            ########   odam.nl   */
/*                                                                            */
/* ************************************************************************** */

#include "libuseful.h"

void	uf_bzero(void *ptr, int len)
{
	uf_memset(ptr, 0, len);
}

void	*uf_memset(void *ptr, char ch, int len)
{
	char	*str;
	int		i;

	str = ptr;
	i = 0;
	while (i < len)
	{
		str[i] = ch;
		i++;
	}
	return (ptr);
}
