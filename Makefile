# **************************************************************************** #
#                                                                              #
#                                                              ::::::::        #
#    Makefile                                                :+:    :+:        #
#                                                           +:+                #
#    By: nekonoor <nekonoor@protonmail.com>                +#+                 #
#                                                         +#+                  #
#    Created: 2019/09/02 20:51:55 by nekonoor            #+#    #+#            #
#    Updated: 2019/09/04 17:43:54 by nekonoor            ########   odam.nl    #
#                                                                              #
# **************************************************************************** #

INC = ./include/
CC = clang-8
CFLAGS = -g -Wall -Wextra -Werror -I$(INC)
TEST = test_libuf.c
SRC = uf_memory.c uf_output.c uf_string.c uf_xtox.c
ODIR = ./obj/
OBJ = $(addprefix $(ODIR),$(SRC:.c=.o))
NAME = libuseful.a
vpath %.c ./src

.PHONY: clean fclean

all: $(NAME)

$(NAME): $(OBJ)
	ar rc $(NAME) $(OBJ)
	ranlib $(NAME)

$(ODIR):
	mkdir $@

$(ODIR)%.o: %.c $(ODIR)
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	$(RM) -r $(ODIR)

fclean: clean
	$(RM) $(NAME)

re: fclean all
